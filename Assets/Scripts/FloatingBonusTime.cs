﻿using UnityEngine;
using System.Collections;

public class FloatingBonusTime : MonoBehaviour 
{
	Color startColor;
	Color endColor;
	float dt;
	TextMesh tm;

	void Awake()
	{
		tm = GetComponent<TextMesh>();
		startColor = tm.color;
		endColor = new Color(startColor.r, startColor.g, startColor.b, 0);
		dt = 0;
		Invoke("ByeBye", 3.5f);
	}

	void Update () 
	{
		tm.color = Color.Lerp(startColor, endColor, dt);
		dt += Time.deltaTime / 3.5f;
		float speed = 0.2f;
		gameObject.transform.position = new Vector3(
			transform.position.x - speed * Time.deltaTime,
			transform.position.y + speed * Time.deltaTime,
			transform.position.z - speed * Time.deltaTime);
	}

	void ByeBye()
	{
		Destroy(gameObject);
	}
}
