﻿using UnityEngine;
using System.Collections;

public class Tile_Script : MonoBehaviour 
{
	public int shapeId;
	
	private bool bFlip = false;
	private bool bFlipBack = false;
	private bool bFlipped = false;
	private bool bMatched = false;
	private bool bUnMatched = false;
	private bool bShowScore = false;
	private bool bLastMatched = false;
	private bool bNoMatch = false;
	private bool bPermanentNoMatch = false;
	private bool bCleared = false;
	private int count = 0;
	private float startTime;
	private GameObject myMatch;
	private Vector3 startPosition;
	private Vector3 endPosition;
	private Quaternion startRotation;
	private Quaternion endRotation;
	private GameObject sparkle;

	private float flipBackDelay = 1.0f;

	// Use this for initialization
	void Start () 
	{
		startPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);
	}
	
	// Update is called once per frame
	void Update () 
	{
		float speed = 2F;	
		float speed2 = 1;	
		
		if (bFlip) 
		{
			transform.rotation = Quaternion.Slerp (Quaternion.Euler (0, 0, 0), Quaternion.Euler (0, 180, 0), (Time.time - startTime) * speed);
			
			//If rotation is completed
			if (Quaternion.Angle(transform.rotation, Quaternion.Euler (0, 180, 0)) == 0.0f) 
			{		
				EndFlip();
			}
		} 
		else if (bFlipBack) 
		{
			transform.rotation = Quaternion.Slerp (Quaternion.Euler (0, 180, 0), Quaternion.Euler (0, 0, 0), (Time.time - startTime) * speed);
			
			//If rotation is completed
			if (Quaternion.Angle(transform.rotation, Quaternion.Euler (0, 0, 0)) == 0.0f) 
			{			
				//Debug.Log("FLIPPED " + Time.time);	
				bNoMatch = false;
				bFlipBack = false;
				bFlip = false;
				bFlipped = false;
			}
		}
		else if(bShowScore)
		{		
			//transform.position = Vector3.Slerp(startPosition, newPosition, (Time.time - startTime) * speed);
			transform.rotation = Quaternion.Slerp( 
				startRotation, 
			    endRotation, 
			    (Time.time - startTime) * speed2);

			transform.position = Vector3.Slerp(
				startPosition, 
				endPosition, 
				(Time.time - startTime) * speed2);

			//Debug.Log(Vector3.Distance(transform.position, endPosition));
			if(transform.position == endPosition)
			{
				bShowScore = false;
				bCleared = true;
				transform.parent = GameObject.FindGameObjectWithTag("MatchBoard").transform;				
			}
		}		
	}

	void EndFlip()
	{
		bFlip = false;	
		bFlipped = true;	
		if(bUnMatched)
		{
			StartCoroutine(StartBackFlip(false)); 
			
			Tile_Script ts = myMatch.GetComponent<Tile_Script>();
			while(ts.myMatch)
			{				
				StartCoroutine(ts.StartBackFlip(true));
				
				Tile_Script nextTs = ts.myMatch.GetComponent<Tile_Script>();
				ts.myMatch = null;
				ts = nextTs;
			}
			StartCoroutine(ts.StartBackFlip(ts != myMatch.GetComponent<Tile_Script>()));
			myMatch = null;
		}
		else if(bLastMatched)
		{
			StartCoroutine(StartBackFlip(true)); 
			
			Tile_Script ts = myMatch.GetComponent<Tile_Script>();
			while(ts.myMatch)
			{				
				StartCoroutine(ts.StartBackFlip(true));
				
				Tile_Script nextTs = ts.myMatch.GetComponent<Tile_Script>();
				ts.myMatch = null;
				ts = nextTs;
			}
			StartCoroutine(ts.StartBackFlip(true));
			myMatch = null;
		}
		else if(bNoMatch)
		{
			if(!bPermanentNoMatch)
				StartCoroutine(StartBackFlip(false)); 
			//Debug.Log(bPermanentNoMatch);
		}

		if(bMatched)
		{
			GameObject matchBoard = GameObject.FindGameObjectWithTag("MatchBoard");
			Match_Script ms = matchBoard.GetComponent<Match_Script>();
			
			Vector3 sparklePosition = new Vector3(
				transform.position.x,
				transform.position.y,
				transform.position.z - 0.5f);			

			sparkle = (GameObject)GameObject.Instantiate(
				ms.sparkleAnimation, 
				sparklePosition, 
				transform.rotation);
			sparkle.transform.parent = gameObject.transform;
			sparkle.SetActive(true);			

			if(myMatch)
			{	
				Tile_Script myMatchTileScript = myMatch.GetComponent<Tile_Script>();
				sparklePosition = new Vector3(
					myMatch.transform.position.x,
					myMatch.transform.position.y,
					myMatch.transform.position.z - 0.5f);					
				
				myMatchTileScript.sparkle = (GameObject)GameObject.Instantiate(
					ms.sparkleAnimation, 
					sparklePosition, 
					myMatch.transform.rotation);

				myMatchTileScript.sparkle.transform.parent = myMatch.transform;
				myMatchTileScript.sparkle.SetActive(true);				
			}				
		}
	}

	public void StartFlipNoMatch(bool permanent = false)
	{
		/*
		private bool bFlip = false;
		private bool bFlipBack = false;
		private bool bFlipped = false;
		private bool bMatched = false;
		private bool bUnMatched = false;
		private bool bShowScore = false;
		private bool bLastMatched = false;
		private bool bNoMatch = false;
		*/
		bNoMatch = true;
		bMatched = false;
		bUnMatched = false;
		bLastMatched = false;
		bFlipBack = false;
		bShowScore = false;
		
		if(!bCleared)	
		{
			bFlip = true;		
			bFlipped = false;
			startTime = Time.time;			
			flipBackDelay = 0.25f;
		}
		else
		{
			bFlipped = true;			
			bFlip = false;			
		}
		
		bPermanentNoMatch = permanent;
	}

	public IEnumerator StartBackFlip(bool bMatched2)
	{		
		yield return new WaitForSeconds(flipBackDelay);
		
		if(!bPermanentNoMatch)
		{
			if(bMatched2)
			{
				bShowScore = true;
				bUnMatched = false;
				bMatched = false;
				bFlipped = true;
				bFlipBack = false;
				bFlip = false;	
	
				GameObject matchBoard = GameObject.FindGameObjectWithTag("MatchBoard");
				
				endPosition = new Vector3(
					matchBoard.transform.position.x, 
					matchBoard.transform.position.y,
					matchBoard.transform.position.z);
				
				int cols = Mathf.RoundToInt(matchBoard.transform.localScale.x * 0.9f);
				//int rows = Mathf.RoundToInt(matchBoard.transform.localScale.y * 0.9f);
	
				//((imageWidth * y) + x) * 4
				int x = Match.matchBoardCount % cols;
				int y = Match.matchBoardCount / cols;			
	
				//endPosition += matchBoard.transform.up * (matchBoard.transform.localScale.y / 2 - 0.6f - Match.matchBoardCount * 1.1f);
				endPosition += matchBoard.transform.up * (matchBoard.transform.localScale.y / 2 - 0.55f - (float)y * 1.1f);
				endPosition += matchBoard.transform.right * (matchBoard.transform.localScale.x / 2 - 0.55f - (float)x * 1.1f);
				endPosition += matchBoard.transform.forward * 0.1f;

				startRotation = Quaternion.Euler (0, 180, 0);
				endRotation = matchBoard.transform.rotation;
	
				Match.matchBoardCount++;					
	
				Invoke("StopSparkles", 3);
			}
			else
			{
				bUnMatched = false;
				bMatched = false;
				bFlipped = true;
				bFlipBack = true;
				bFlip = false;	
			}
			
			startTime = Time.time;	
		}
	}

	void StopSparkles()
	{
		//Debug.Log(sparkle);		
		//Destroy(sparkle);
		if(sparkle)
		{
			foreach(Transform child in sparkle.transform)
			{		
				// Component epe = child.GetComponent("EllipsoidParticleEmitter");
				// if(epe != null)
				// 	epe.GetComponent<ParticleEmitter>().emit = false;
			}
		}
		Invoke("DestroySparkles", 3);
	}

	void DestroySparkles()
	{
		Destroy(sparkle);
	}

	void OnMouseDown()
	{
		if (!bFlip && !bFlipped && LevelNumber.gameRunning) 
		{
			flipBackDelay = 1.0f;

			if(Match.shapeId == -1)
			{
				Match.shapeId = shapeId;
				Match.lastMatch = gameObject;
				Match.bFirstMatch = true;
				//Match.matches.Add(gameObject);
				Match.currentMatchCount = 0;
			}
			else if(Match.shapeId == shapeId)
			{
				bMatched = true;
				Match.currentMatchCount++;
				MatchCount mc = Match.maxMatches[shapeId];

				//Count the first match an extra time
				if(Match.bFirstMatch)
					mc.count++;

				Match.bFirstMatch = false;

				//Debug.Log(Match.currentMatchCount + " Match " + shapeId + "_" + mc.count + "_" + mc.max);
				myMatch = Match.lastMatch;
				
				if(mc.count == mc.max)
				{
					//myMatch = Match.lastMatch;
					Match.shapeId = -1;
					bLastMatched = true;
					mc.bCleared = true;
					AddBonusTime(gameObject);
					Match.lastMatch = null;
					DetectLevelCleared();
				}
				else
				{
					//myMatch = Match.lastMatch;
					Match.lastMatch = gameObject;
				}
				mc.count++;
				//Match.matches.Add(gameObject);
			}
			else
			{
				if(Match.currentMatchCount > 0)
				{
					AddBonusTime(Match.lastMatch);
					MatchCount mc = Match.maxMatches[Match.lastMatch.GetComponent<Tile_Script>().shapeId];
					//Debug.Log(mc.count + "_" + mc.max);
					if(mc.count == mc.max)
					{	
						mc.bCleared = true;
						DetectLevelCleared();
					}
				}
				Match.currentMatchCount = 0;

				bMatched = false;
				bUnMatched = true;	
				myMatch = Match.lastMatch;
				Match.shapeId = -1;
				Match.lastMatch = null;				
			}
	
			bFlip = true;

			count++;
			startTime = Time.time;
		}
	}

	void DetectLevelCleared()
	{
		bool bCleared = true;
		//Debug.Log("*******************************");
		foreach(int key in Match.maxMatches.Keys)
		{	
			//Debug.Log(key + "_" + Match.maxMatches[key].bCleared);
			bCleared &= Match.maxMatches[key].bCleared;
		}
		
		if(bCleared)
		{
			//Timer_Script ts = GameObject.FindGameObjectWithTag("TimerText").GetComponent<Timer_Script>();
			//ts.bRunning = false;	
			LevelNumber.gameRunning = false;
			GameObject.FindGameObjectWithTag("MatchBoard").GetComponent<Match_Script>().StartLevelClearedAnimation();
			if(LevelNumber.number >	GameData_Script.game.lastLevel)
				GameData_Script.game.lastLevel++;
		}
	}

	void AddBonusTime(GameObject lastMatch)
	{
		//Debug.Log(Match.bonuseTimeDefault);
		//Debug.Log(Match.currentMatchCount);
		float bonusTime = Match.bonuseTimeDefault * (Match.currentMatchCount + 1);
		//Debug.Log(bonusTime);
		string bonusTimeText = bonusTime.ToString();
		GameObject.FindGameObjectWithTag("TimerClock").GetComponent<Timer_Script>().AddBonusTime(bonusTime);

/*
		float currentBonusTime = GameObject.FindGameObjectWithTag("TimerClock").GetComponent<Timer_Script>().bonusTime;
		
		float oldBonusTime = 0.0f;
		if(currentBonusTime > 0)
			oldBonusTime = Match.bonusTime;
		Match.bonusTime = Match.bonuseTimeDefault * (Match.currentMatchCount + 1);
		GameObject.FindGameObjectWithTag("TimerClock").GetComponent<Timer_Script>().realBonusTime += Match.bonusTime;
		currentBonusTime += Match.bonusTime;
		if(oldBonusTime > 0)
			Match.bonusTime += oldBonusTime;
		GameObject.FindGameObjectWithTag("TimerClock").GetComponent<Timer_Script>().bonusTime = currentBonusTime;
		*/

		GameObject fbt = (GameObject)GameObject.Instantiate(
			GameObject.FindGameObjectWithTag("MatchBoard").GetComponent<Match_Script>().floatingBonusTimeText);
		fbt.GetComponent<TextMesh>().text = "+" + bonusTime.ToString();
		fbt.transform.position = new Vector3(
			lastMatch.transform.position.x, 
			lastMatch.transform.position.y, 
			-0.1f);
	}
}
