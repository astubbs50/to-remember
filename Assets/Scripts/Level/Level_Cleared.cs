﻿using UnityEngine;
using System.Collections;

public class Level_Cleared : MonoBehaviour {

	//private Quaternion endRotation;
	//private float zRot = 0;

	void Start() 
	{
		//endRotation = Quaternion.Euler(0, 0, 185);
	}

	void Update () 
	{
		transform.position = Vector3.Slerp(transform.position, new Vector3(0, 0.75f, -3), Time.deltaTime);
		if(Vector3.Distance(transform.position, new Vector3(0, 0, 0)) < 8)
		{
			transform.rotation = Quaternion.Slerp(
				transform.rotation, 
				Quaternion.Euler(0, 0, 0), 
				Time.deltaTime * 2);
		}
		else
		{			
			transform.Rotate(new Vector3(0,0,5));
		}
		
		
		//if(transform.rotation.eulerAngles.z == 185)
		//	endRotation = Quaternion.Euler(0 , 0, 360);
	}
}
