﻿using UnityEngine;
using System.Collections;

public class Timer_Script : MonoBehaviour 
{
	public float countDown;
	public bool bLightColors;
	public float startTime;
	
	
	public TextMesh timerText;
	public TextMesh timerText2;
	public GameObject White1;
	public GameObject White2;
	public GameObject Red1;
	public GameObject Red2;
	
	private float realTime;
	public float realBonusTime;
	private float bonusTime;
	private string bonusTimeString;

	// Use this for initialization
	void Start () 
	{		
		if(bLightColors)
		{
			timerText.color = new Color(1.0f, 0.1f, 0.2f);
			timerText2.color = new Color(0.8f, 1.0f, 0.2f);
		}

		countDown = startTime;
		bonusTime = 0;
	}
	
	// Update is called once per frame
	void Update () 
	{
		UpdateTime();
	}

	void UpdateTime()
	{
		float bonusAdder = Time.deltaTime;
		
		if(LevelNumber.gameRunning && bonusTime <= 0)
		{
			countDown -= Time.deltaTime;
			realTime += Time.deltaTime;
		}

		if(countDown >= 100)
			timerText.text = countDown.ToString("F0");
		else if(countDown >= 10)
			timerText.text = countDown.ToString("F1");
		else
			timerText.text = countDown.ToString("F2");
		
		
		if(LevelNumber.gameRunning && bonusTime > 0)
		{
			
			//Debug.Log("1: " + countDown);

			////timerText.text += "\n+" + Match.bonusTime.ToString("F1");
			timerText2.text = "+" + bonusTime.ToString("F1");

			////scoreText.text = bonusTime.ToString("F1");
			////scoreText.gameObject.SetActive(true);
			//countDown += bonusAdder;
			bonusTime -= bonusAdder;
			//Debug.Log("bt: " + bonusTime + " ba: " + bonusAdder);
			//Debug.Log("2:" + countDown);
		}		
		else
		{
			timerText2.text = "";
			bonusTime = 0;
		}
		
		UpdateClock();
		if (countDown <= 0)
		{
			countDown = 0;
			if(LevelNumber.gameRunning)
			{
				GameObject matchBoard = GameObject.FindGameObjectWithTag("MatchBoard");
				Match_Script ms = matchBoard.GetComponent<Match_Script>();
				ms.StartGameOverAnimation();
			}
			LevelNumber.gameRunning = false;
		}
	}

	public void AddBonusTime(float _bonusTime)
	{
		bonusTimeString = _bonusTime.ToString();
		bonusTime += _bonusTime;
	}

	void UpdateClock()
	{
		float pct = countDown / startTime;

		//Debug.Log (pct);
		if (pct >= 1.0f) 
		{
			White1.transform.localPosition = new Vector3 (0, 0, -0.002f);
			White2.transform.localPosition = new Vector3 (0, 0, -0.002f);
			Red1.transform.localPosition = new Vector3 (0, 0, 0.002f);
			Red2.transform.localPosition = new Vector3 (0, 0, 0.002f);
			White1.transform.localRotation = Quaternion.Euler (0, 0, 0);
			White2.transform.localRotation = Quaternion.Euler (0, 0, 180);
		} 
		else if (pct > 0.5f) 
		{
			Red2.transform.localPosition = new Vector3 (0, 0, 0.001f);	
			White1.transform.localPosition = new Vector3 (0, 0, 0.001f);
			Red1.transform.localPosition = new Vector3 (0, 0, -0.002f);
			White2.transform.localPosition = new Vector3 (0, 0, -0.003f);

			//Red2.transform.localPosition = new Vector3(0, 0, 0f);
			//White1.transform.localPosition = new Vector3(0, 0, -1f);
			//Red1.transform.localPosition =  new Vector3(0, 0, -2f);
			//White2.transform.localPosition =  new Vector3(0, 0, -3f);

			White1.transform.localRotation = Quaternion.Euler (0, 0, 0);
			White2.transform.localRotation = Quaternion.Euler (0, 0, 180);
			Red1.transform.localRotation = Quaternion.Euler (0, 0, pct * 360);
			Red2.transform.localRotation = Quaternion.Euler (0, 0, 0);
		} 
		else if (pct > 0.0f) 
		{
			White2.transform.localPosition = new Vector3 (0, 0, 0.001f);
			White1.transform.localPosition = new Vector3 (0, 0, 0.001f);
			Red2.transform.localPosition = new Vector3 (0, 0, -0.001f);
			Red1.transform.localPosition = new Vector3 (0, 0, -0.002f);

			White1.transform.localRotation = Quaternion.Euler (0, 0, 0);
			White2.transform.localRotation = Quaternion.Euler (0, 0, 180);
			Red1.transform.localRotation = Quaternion.Euler (0, 0, pct * 360);
			Red2.transform.localRotation = Quaternion.Euler (0, 0, 180);
		} 
		else 
		{
			White1.transform.localPosition = new Vector3 (0, 0, 0.002f);
			White2.transform.localPosition = new Vector3 (0, 0, 0.002f);
			Red1.transform.localPosition = new Vector3 (0, 0, -0.002f);
			Red2.transform.localPosition = new Vector3 (0, 0, -0.002f);
			White1.transform.localRotation = Quaternion.Euler (0, 0, 0);
			White2.transform.localRotation = Quaternion.Euler (0, 0, 180);
			Red1.transform.localRotation = Quaternion.Euler (0, 0, 0);
			Red2.transform.localRotation = Quaternion.Euler (0, 0, 180);
		}
	}
}
