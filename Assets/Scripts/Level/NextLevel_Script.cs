﻿using UnityEngine;
using System.Collections;

public class NextLevel_Script : MonoBehaviour 
{

	private Vector3 endPosition;
	private Quaternion endRotation;
	private Vector3 startPosition;
	private Quaternion startRotation;
	private bool bMouseDownStarted = false;
	
	public bool nextLevel = true;

	// Use this for initialization
	void Start () 
	{
		if(nextLevel)
			endPosition = new Vector3(0, -1, -2);
		else
			endPosition = new Vector3(0, -2, -2);

		endRotation = Quaternion.Euler(0, 180, 0);
		
		bMouseDownStarted = false;
	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.position = Vector3.Slerp(transform.position, endPosition, Time.deltaTime);
		transform.rotation = Quaternion.Slerp(transform.rotation, endRotation, Time.deltaTime * 8);
	}
	
	void OnMouseDown()
	{
		if(!bMouseDownStarted)
		{
			startPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);	
			startRotation = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
			endPosition = new Vector3(transform.position.x, transform.position.y - 0.25f, transform.position.z - 0.25f);	
			endRotation = Quaternion.Euler(transform.rotation.eulerAngles.x - 25f, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
		}
		
		bMouseDownStarted = true;
	}
	
	void OnMouseUp()
	{
		endPosition = startPosition;
		endRotation = startRotation;
		bMouseDownStarted = false;
		if(nextLevel)
			Application.LoadLevel(LevelNumber.number + 1);
		else
			Application.LoadLevel(LevelNumber.number);
	}
}
