﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Match_Script : MonoBehaviour 
{
	public GameObject sparkleAnimation;
	public GameObject levelCleared;
	public GameObject gameOver;
	public GameObject nextLevelButton;
	public GameObject playAgainButton;	
	public GameObject floatingBonusTimeText;

	public GameObject[] stars;

	private bool bWin = false;
	private Vector3 startPosition;
	private Quaternion startRotation;
	private Vector3 endPosition;
	private Quaternion endRotation;

	
	private float startTime;
	private float startTime2;
	private bool bFlipped = false;

	void Awake()
	{	
		Match.shapeId = -1;
		//public static List<GameObject> matches = new List<GameObject>();
		Match.lastMatch = null;
		Match.maxMatches = new Dictionary<int, MatchCount>();
		Match.matchBoardCount = 0;	
		Match.currentMatchCount = 0;
		Match.bonusTime = 1.5f;
		Match.bonuseTimeDefault = 1.5f;	
		Match.matchBoardCount = 0;
		
		CountAllTiles();
	}

	void CountAllTiles()
	{
		GameObject[] gameObjects = GameObject.FindGameObjectsWithTag("Tile");
		foreach(GameObject go in gameObjects)
		{
			Tile_Script ts = go.GetComponent<Tile_Script>();
			MatchCount mac;
			
			if(Match.maxMatches.ContainsKey(ts.shapeId))
			{
				mac = Match.maxMatches[ts.shapeId];
				mac.max++;
			}
			else
			{
				mac = new MatchCount();
				mac.max = 0;
				mac.count = 0;
				mac.bCleared = false;
				Match.maxMatches.Add(ts.shapeId, mac);
			}
		}
	}

	void Update()
	{
		if(bWin && !bFlipped)
		{
			float speed = 1.0f;
			transform.position = Vector3.Slerp(startPosition, endPosition, (Time.time - startTime) * speed);			
			transform.rotation = Quaternion.Slerp(startRotation, endRotation, (Time.time - startTime2) * speed * 2);
			//Debug.Log(Quaternion.Dot(transform.rotation, endRotation));
			
			if(transform.rotation.eulerAngles.y == 0)
			//if(Quaternion.Dot(transform.rotation, endRotation) == -1)
			{				
				startTime2 = Time.time;
				startRotation = transform.rotation;
				endRotation = Quaternion.Euler(0, 180, 0);
			}
			
			if(transform.position == endPosition)
			{
				bFlipped = true;
				Invoke("StartExplosionAnimation", 0.5f);
			}
		}
	}
	
	public void StartExplosionAnimation()
	{
		GameObject[] tiles = GameObject.FindGameObjectsWithTag("Tile");
		foreach(GameObject tile in tiles)
		{
			//tile.AddComponent<Rigidbody>();
			//tile.rigidbody.useGravity = false;
			//tile.rigidbody.inertiaTensor = new Vector3(1, 1, 1);
			//tile.transform.GetChild(0).transform.position = new Vector3(0, 0, 0.52f);
			tile.transform.parent = null;
			tile.GetComponent<Rigidbody>().isKinematic = false;
			tile.GetComponent<Rigidbody>().useGravity = true;
			tile.GetComponent<Rigidbody>().AddExplosionForce(
				6,
				new Vector3(0, 0, 2),
				50,
				2,
				ForceMode.Impulse
				);
		}
		
		GetComponent<Rigidbody>().isKinematic = false;
		GetComponent<Rigidbody>().useGravity = true;
		GetComponent<Rigidbody>().AddExplosionForce(
			6,
			new Vector3(1, 1, -1),
			50,
			1,
			ForceMode.Impulse
			);
		//GameObject.FindGameObjectWithTag("LevelCleared").SetActive(true);
		Invoke("StartLevelClearedTextAnimation", 1.0f);
	}

	public void StartGameOverAnimation()
	{
		GameObject[] goTiles = GameObject.FindGameObjectsWithTag("Tile");
		foreach(GameObject tile in goTiles)
		{
			Tile_Script ts = tile.GetComponent<Tile_Script>();	
			ts.StartFlipNoMatch(true);
			
			//tile.renderer.
			//ts.GetComponent<Material>().SetColor(0, Color.clear);
			//FadeOut(ts);
			StartCoroutine("FadeOut", ts);	
		}
		gameOver.SetActive(true);
		playAgainButton.SetActive(true);
	}

	private IEnumerator FadeOut(Tile_Script ts)
	{
		float r = ts.GetComponent<Renderer>().material.color.r;
		float g = ts.GetComponent<Renderer>().material.color.g;
		float b = ts.GetComponent<Renderer>().material.color.b;
		while(ts.GetComponent<Renderer>().material.color.b > 0.5)
		{
			r -= 0.02f;
			g -= 0.02f;
			b -= 0.02f;
			ts.GetComponent<Renderer>().material.SetColor("_Color", new Color(r, g, b));
			ts.transform.GetChild(0).gameObject.GetComponent<Renderer>().material.SetColor("_Color", ts.GetComponent<Renderer>().material.color);
			
			yield return new WaitForSeconds(0.1f);
		}
	}

	public void StartLevelClearedTextAnimation()
	{
		levelCleared.SetActive(true);
		nextLevelButton.SetActive(true);
		playAgainButton.SetActive(true);
	}

	public void StartLevelClearedAnimation()
	{
		Invoke("StartAnimation", 3.0f);
	}

	private void StartAnimation()
	{
		startPosition = new Vector3(
			transform.position.x,
			transform.position.y,
			transform.position.z
		);
		
		startRotation = Quaternion.Euler(
			transform.rotation.eulerAngles.x,
			transform.rotation.eulerAngles.y,
			transform.rotation.eulerAngles.z
		);

		endPosition = new Vector3(0, 0, -1);
		
		endRotation = Quaternion.Euler(0, 0, 270);
		startTime = Time.time;
		startTime2 = Time.time;
		bWin = true;

		Invoke("ShowStars", 3.0f);
	}

	private void ShowStars()
	{
		float timeLeft = GameObject.FindGameObjectWithTag("TimerClock").GetComponent<Timer_Script>().countDown;
		LevelNumber level = GameObject.FindGameObjectWithTag("LevelNumber").GetComponent<LevelNumber>();
		int starsCount = 0;
		if(timeLeft > level.starScore1)
		{
			stars[0].SetActive(true);
			starsCount++;
		}
		if(timeLeft > level.starScore2)
		{
			stars[1].SetActive(true);
			starsCount++;
		}
		if(timeLeft > level.starScore3)
		{
			stars[2].SetActive(true);
			starsCount++;
		}
		
		if(GameData_Script.game.starsCount.Count < level.levelNumber)
		{
			GameData_Script.game.starsCount.Add(0);
		}
		
		if(starsCount > GameData_Script.game.starsCount[level.levelNumber - 1])
			GameData_Script.game.starsCount[level.levelNumber - 1] = starsCount;

		GameData_Script.game.SaveData();
	}
}



public static class Match
{
	public static int shapeId = -1;
	//public static List<GameObject> matches = new List<GameObject>();
	public static GameObject lastMatch;
	public static Dictionary<int, MatchCount> maxMatches = new Dictionary<int, MatchCount>();
	public static int matchBoardCount = 0;	
	public static int currentMatchCount = 0;
	public static float bonusTime = 1.5f;
	public static float bonuseTimeDefault = 1.5f;
	public static bool bFirstMatch = false;
}

public class MatchCount
{
	public int max;
	public int count;
	public bool bCleared;
}