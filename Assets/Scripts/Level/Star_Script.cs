﻿using UnityEngine;
using System.Collections;

public class Star_Script : MonoBehaviour {

	public Vector3 endPosition;
	public Quaternion endRotation;

	// Update is called once per frame
	void Update () {
		
		transform.position = Vector3.Slerp(transform.position, endPosition, Time.deltaTime);
		transform.rotation = Quaternion.Slerp(transform.rotation, endRotation, Time.deltaTime);
	}
}
