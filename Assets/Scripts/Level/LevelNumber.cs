﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

enum ShowModes
{
	ByShape = 0,
	ByColumns = 1,
	ByRows = 2,
	ByDiaginol = 3,
	ByCenterCircle = 4,
	ShowNone = 5
}

public class LevelNumber : MonoBehaviour 
{
	public static int number;
	public static bool gameRunning = false;
	public GameObject readyText;
	public float starScore1;
	public float starScore2;
	public float starScore3;

	public int levelNumber;
	public int showMode;

	void Awake()
	{
		gameRunning = false;
		number = levelNumber;
		ShuffleTiles();
	}

	void ShuffleTiles()
	{
		GameObject[] goTiles = GameObject.FindGameObjectsWithTag("Tile");
		foreach(GameObject tile in goTiles)
		{
			GameObject target = goTiles[ Random.Range (0, goTiles.Length - 1) ];
			Vector3 tempVector = tile.transform.position;
			tile.transform.position = target.transform.position;
			target.transform.position = tempVector;
		}

		StartCoroutine(ShowTiles());
	}

	IEnumerator ShowTiles()
	{
		float a = 0.01f;
		float d = 0.2f;
		TextMesh tmReady = readyText.GetComponent<TextMesh>();
		while(a > 0)
		{		
			tmReady.color = new Color(1, 0, 0, a);	
			a += d;
			//Debug.Log(a);
			if(a >= 1)
			{
				d = -d;
				//Debug.Log(d);
				a += d;
			}

			yield return new WaitForSeconds(0.1f);
		}

		GameObject[] goTiles = GameObject.FindGameObjectsWithTag("Tile");
		if(showMode == (int)ShowModes.ByShape)
		{
			int startShapeId = goTiles[0].GetComponent<Tile_Script>().shapeId;
			int nextShapeId = -1;
			List<int> usedShapes = new List<int>();

			while(startShapeId != -1)
			{
				foreach(GameObject tile in goTiles)
				{
					Tile_Script ts = tile.GetComponent<Tile_Script>();
					if(startShapeId == ts.shapeId)
					{
						ts.StartFlipNoMatch();
					}
					else if(nextShapeId == -1 && !usedShapes.Exists(e => e == ts.shapeId))
					{
						nextShapeId = ts.shapeId;
					}		
				}
				
				usedShapes.Add(startShapeId);
				startShapeId = nextShapeId;
				nextShapeId = -1;
				
				yield return new WaitForSeconds(1f);
			}
		}
		yield return new WaitForSeconds(0.5f);
		tmReady.text = "Go!";
		a = 0.01f;
		d = 0.2f;
		while(a > 0)
		{		
			readyText.GetComponent<TextMesh>().color = new Color(1, 0, 0, a);	
			a += d;
			//Debug.Log(a);
			if(a >= 1)
			{
				d = -d;
				//Debug.Log(d);
				a += d;
			}
			
			yield return new WaitForSeconds(0.1f);
		}
		gameRunning = true;
	}
}
