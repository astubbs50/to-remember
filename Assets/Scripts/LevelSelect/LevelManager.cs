﻿using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour 
{
	public static LevelManager man;
	public GameObject[] chapters;
	public int selectedChapter;
	
	void Awake()
	{
		selectedChapter = 0;
		if(man == null)
		{
			man = this;
		}
		else if(man != this)
		{
			Destroy(gameObject);
		}	
	}

	public void EnableSelectedLevels()
	{
		float x = 0;
		for(int i = selectedChapter - 1; i >= 0; i--)
		{
			x -= 10.0f;
			chapters[i].GetComponent<Chapter_Script>().endPosition = new Vector3(x, 5f, 0);			
		}
		
		chapters[selectedChapter].GetComponent<Chapter_Script>().endPosition = new Vector3(0, 5f, 0);

		x = 0;
		for(int i = (int)selectedChapter + 1; i < chapters.Length; i++)
		{
			x += 10.0f;
			chapters[i].GetComponent<Chapter_Script>().endPosition = new Vector3(x, 5f, 0);			
		}
	}
}
