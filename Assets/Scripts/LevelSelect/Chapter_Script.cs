﻿using UnityEngine;
using System.Collections;

public class Chapter_Script : MonoBehaviour 
{
	public Vector3 endPosition;
	
	// Use this for initialization
	void Start () 
	{
		endPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.position = Vector3.Slerp(transform.position, endPosition, Time.deltaTime);
	}
}
