﻿using UnityEngine;
using System.Collections;

public class QuitButton_Script : MonoBehaviour 
{

	public Vector3 endPosition;
	public Quaternion endRotation;
	public Vector3 startPosition;
	public Quaternion startRotation;	
	
	// Use this for initialization
	void Start () 
	{
		endPosition = transform.position;
		endRotation = transform.rotation;
		startPosition = transform.position;
		startRotation = transform.rotation;
	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.position = Vector3.Slerp(transform.position, endPosition, Time.deltaTime);
		transform.rotation = Quaternion.Slerp(transform.rotation, endRotation, Time.deltaTime);
	}
	
	void OnMouseDown()
	{		
		if(endPosition == startPosition)
		{
			endPosition = new Vector3(
				startPosition.x, 
				startPosition.y - 0.2f, 
				startPosition.z - 0.1f
			);		
			endRotation = Quaternion.Euler(
				startRotation.eulerAngles.x - 15, 
				startRotation.eulerAngles.y,
				startRotation.eulerAngles.z
			);
		}
	}
	
	void OnMouseUp()
	{
		endPosition = startPosition;
		endRotation = startRotation;
		Application.Quit();
	}
}
