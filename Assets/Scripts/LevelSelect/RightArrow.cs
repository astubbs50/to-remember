﻿using UnityEngine;
using System.Collections;

public class RightArrow : MonoBehaviour 
{

	public Vector3 endPosition;
	private Quaternion endRotation;
	private Vector3 startPosition;
	private Quaternion startRotation;

	public void UpdateEndPosition(Vector3 newPosition)
	{
		endPosition = newPosition;
		startPosition = endPosition;
	}

	// Use this for initialization
	void Start () 
	{
		endPosition = transform.position;
		endRotation = transform.rotation;
		startPosition = transform.position;
		startRotation = transform.rotation;
	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.position = Vector3.Slerp(transform.position, endPosition, Time.deltaTime);
		transform.rotation = Quaternion.Slerp(transform.rotation, endRotation, Time.deltaTime);
	}
	
	void OnMouseDown()
	{
		endPosition = new Vector3(
			startPosition.x, 
			startPosition.y - 0.2f, 
			startPosition.z - 0.1f
			);		
		endRotation = Quaternion.Euler(
			startRotation.eulerAngles.x, 
			startRotation.eulerAngles.y,
			startRotation.eulerAngles.z - 15
			);
	}

	void OnMouseUp()
	{
		endPosition = startPosition;
		endRotation = startRotation;	
		if(LevelManager.man.selectedChapter < LevelManager.man.chapters.Length - 1)
		{
			LevelManager.man.selectedChapter++;
			LevelManager.man.EnableSelectedLevels();
		}	
	}
}
