﻿using UnityEngine;
using System.Collections;

public class PlayButton_Script : MonoBehaviour 
{
	//public GameObject levelTile;
	//public GameObject levelPopcorn;

	//public GameObject quit;
	public GameObject leftArrow;
	public GameObject rightArrow;

	private Vector3 endPosition;
	private Quaternion endRotation;
	private Vector3 startPosition;
	private Quaternion startRotation;	

	// Use this for initialization
	void Start () 
	{
		endPosition = transform.position;
		endRotation = transform.rotation;
		startPosition = transform.position;
		startRotation = transform.rotation;
	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.position = Vector3.Slerp(transform.position, endPosition, Time.deltaTime);
		transform.rotation = Quaternion.Slerp(transform.rotation, endRotation, Time.deltaTime);
	}
	
	void OnMouseDown()
	{		
		endPosition = new Vector3(
			startPosition.x, 
			startPosition.y - 0.2f, 
			startPosition.z - 0.1f
		);		
		endRotation = Quaternion.Euler(
			startRotation.eulerAngles.x - 15, 
			startRotation.eulerAngles.y,
			startRotation.eulerAngles.z
		);
	}
	
	void OnMouseUp()
	{
		endPosition = new Vector3(-25, 25, 0);
		endRotation = startRotation;
		
		//QuitButton_Script qbs = quit.GetComponent<QuitButton_Script>();
		
		//qbs.endPosition = new Vector3(-2.5f, 5f, 2.3f);
		//qbs.startPosition = qbs.endPosition;

		LevelManager.man.EnableSelectedLevels();
	
		leftArrow.GetComponent<LeftArrow>().UpdateEndPosition(new Vector3(-5,2,0));
		rightArrow.GetComponent<RightArrow>().UpdateEndPosition(new Vector3(5,2,0));		

		/*GameObject[] levels = GameObject.FindGameObjectsWithTag("Level Tile");
		foreach(GameObject level in levels)
		{
			//level.transform.position.z = 0;
			LevelSelect_Script lss = level.GetComponent<LevelSelect_Script>();
			lss.endPosition = new Vector3(
				level.transform.position.x - 10,
				level.transform.position.y - 5,
				0
			);
		}*/
	}

}
