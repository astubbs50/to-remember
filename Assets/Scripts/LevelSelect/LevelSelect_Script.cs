﻿using UnityEngine;
using System.Collections;

public class LevelSelect_Script : MonoBehaviour 
{
	public GameObject[] stars;
	public int levelNumber;
	//private bool bLocked = true;
	
	void Start()
	{
		if(GameData_Script.game.lastLevel + 1 >= levelNumber)
		{
			//bLocked = false;
			//Debug.Log(transform.GetChild(1).ToString());
			Destroy(transform.GetChild (1).gameObject);

			if( GameData_Script.game.starsCount.Count > levelNumber - 1)
			{
				int starsCount = GameData_Script.game.starsCount[levelNumber - 1];
				if(starsCount > 0)
				{
					stars[0].SetActive(true);
				}
				if(starsCount > 1)
				{
					stars[1].SetActive(true);
				}
				if(starsCount > 2)
				{
					stars[2].SetActive(true);
				}
			}			
		}
	}

	void OnMouseDown()
	{
		Application.LoadLevel(levelNumber);
	}
}
