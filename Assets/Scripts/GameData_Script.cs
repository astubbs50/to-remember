﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
//using System.Runtime.Serialization.Formatters.Binary;
//using System.IO;

public class GameData_Script : MonoBehaviour 
{
	public static GameData_Script game;
	
	public int lastLevel;
	public List<int> starsCount;

	private string fileName;

	void Awake()
	{
//		fileName = Application.persistentDataPath + "/data.dat";

		if(game == null)
		{
			DontDestroyOnLoad(gameObject);
			game = this;	
		}
		else if(game != this)
		{
			Destroy(gameObject);
		}		
	}

	void OnEnable()
	{
		if(PlayerPrefs.HasKey("lastLevel"))
			lastLevel = PlayerPrefs.GetInt("lastLevel");
		else
			lastLevel = 0;

		starsCount = new List<int>();

		for(int i = 0; i < lastLevel; i++)
		{
			string strLabel = "starsCount_" + i.ToString();
			if(PlayerPrefs.HasKey(strLabel))
			{
				starsCount.Add(PlayerPrefs.GetInt(strLabel));
			}
			else
			{
				starsCount.Add(0);
			}
		}
		/*if(File.Exists(fileName))
		{
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open(fileName, FileMode.Open);
			GameData data = (GameData)bf.Deserialize(file);
			file.Close();
			lastLevel = data.lastLevel;
			starsCount = data.starsCount;
			if(starsCount == null)
				starsCount = new List<int>();
			while(starsCount.Count <= lastLevel)
			{
				starsCount.Add(0);
			}
		}
		else
		{
			lastLevel = 1;
			starsCount = new List<int>();
		}*/
	}

    public void SaveData()
    {
        PlayerPrefs.SetInt("lastLevel", lastLevel);
        for (int i = 0; i < starsCount.Count; i++)
        {
            PlayerPrefs.SetInt("starsCount_" + i.ToString(), starsCount[i]);
        }
        PlayerPrefs.Save();
    }
	
	void OnDisable()
	{
        //SaveData();

		/*BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Create(fileName);
		GameData data = new GameData();
		data.lastLevel = lastLevel;
		data.starsCount = starsCount;
		
		bf.Serialize(file, data);
		file.Close();*/
	}
}

//[Serializable]
//class GameData
//{
//	public int lastLevel;
//	public List<int> starsCount;
//}